import { ISession } from '../data/sessions.interface';

export class ConfAgendaService {

  private mySessions : ISession[] = [];

  addSessionToAgenda(session : ISession) {
    if (this.mySessions.indexOf(session) === -1) {
      this.mySessions.push(session);
    }

  }

  removeSessionFromAgenda(session : ISession) {
     const position = this.mySessions.findIndex((sessionEl : ISession) => {
       return sessionEl.id==session.id;
     });

     this.mySessions.splice(position, 1);
  }

  doesContainEvent(session : ISession) {
    return (this.mySessions.indexOf(session) != -1);
  }

  getMySessions() {
    return this.mySessions.slice();
  }

}
