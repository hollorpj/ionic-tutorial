
export class SettingsService {

  highlight : boolean = false;

  isHighlighted() {
   return this.highlight;
  }

  setHighlighted(highlighted) {
    this.highlight = highlighted;
  }

}
