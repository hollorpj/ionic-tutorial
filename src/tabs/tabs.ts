import { Component } from '@angular/core';
import { AgendaComponent } from '../components/agenda/agenda';
import { ScheduleComponent } from '../components/schedule/schedule';

  @Component({
    selector : 'page-tabs',
    template :
    `
      <ion-tabs>

        <ion-tab
          [root]="schedule"
          tabTitle="Conf Schedule"
          tabIcon="calendar"></ion-tab>

        <ion-tab
          [root]="agenda"
          tabTitle="My Agenda"
          tabIcon="list"></ion-tab>

      </ion-tabs>
    `
  })
  export class Tabs {
    agenda = AgendaComponent;
    schedule = ScheduleComponent;
  }
