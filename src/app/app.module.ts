import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SecondaryComponent } from '../components/secondary/secondary';
import { ScheduleComponent } from '../components/schedule/schedule';
import { AgendaComponent } from '../components/agenda/agenda';
import { SessionComponent } from '../components/session/session';
import { Tabs }  from '../tabs/tabs';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { ConfAgendaService } from '../services/ConfAgendaService';
import { SettingsService } from '../services/SettingsService';

import { SettingsPage } from '../pages/settings/settings';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SecondaryComponent,
    AgendaComponent,
    SessionComponent,
    ScheduleComponent,
    Tabs,
    SettingsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SecondaryComponent,
    AgendaComponent,
    SessionComponent,
    ScheduleComponent,
    Tabs,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConfAgendaService,
    SettingsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
