import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SecondaryComponent} from '../../components/secondary/secondary';
import { AgendaComponent } from '../../components/agenda/agenda';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private secondaryComponent = AgendaComponent;

  constructor(public navCtrl: NavController) {

  }

  gotoSecondary(name) {
    this.navCtrl.push(AgendaComponent, {'loginName' : name} );
  }

}
