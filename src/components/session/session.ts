import { Component } from '@angular/core';
import { NavParams, AlertController } from 'ionic-angular'
import { ISession } from '../../data/sessions.interface';
import { ConfAgendaService } from '../../services/ConfAgendaService';


/**
 * Generated class for the SessionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'session',
  templateUrl: 'session.html'
})
export class SessionComponent {

  text: string;
  sessionDetails : ISession;

  attending : boolean = true;

  constructor(private navParams : NavParams, private alertCtrl : AlertController, private confAgendaService : ConfAgendaService) {
    this.sessionDetails = this.navParams.data;

    this.ionViewWillEnter();
  }

  ionViewWillEnter() {
    this.attending = this.confAgendaService.doesContainEvent(this.sessionDetails);
  }


  addToAgenda(sessionDetail : ISession) {
    const alert = this.alertCtrl.create({
      title : 'Add To Agenda',
      subTitle : 'Are you sure?',
      message: 'Are you sure you want to add this session to your agenda?',
      buttons : [ {
        text : 'Aye',
        handler : () => {
          this.confAgendaService.addSessionToAgenda(sessionDetail);
          this.attending = true;
        }
      }, {
        text : 'Nah',
        role : 'cancel',
        handler : () => {}
      }]

     });

     alert.present();

  }

   removeFromAgenda(sessionDetail : ISession) {
      const alert = this.alertCtrl.create({
        title : 'Remove From Agenda',
        subTitle : 'Are you sure?',
        message: 'Are you sure you want to remove this session to your agenda?',
        buttons : [ {
          text : 'Aye',
          handler : () => {
            this.confAgendaService.removeSessionFromAgenda(sessionDetail);
            this.attending = false;
          }
        }, {
          text : 'Nah',
          role : 'cancel',
          handler : () => {}
        }]

       });

       alert.present();

    }

}
