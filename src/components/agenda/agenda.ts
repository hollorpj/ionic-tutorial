import { Component } from '@angular/core';
import { ConfAgendaService } from '../../services/ConfAgendaService';
import { ISession } from '../../data/sessions.interface';
import { SessionComponent } from '../session/session';
import { AlertController, NavController } from 'ionic-angular'

/**
 * Generated class for the AgendaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'agenda',
  templateUrl: 'agenda.html'
})
export class AgendaComponent {

  myAgenda : ISession[];
  sessionPage = SessionComponent;

  constructor(private confAgendaService : ConfAgendaService, private alertCtrl : AlertController, private navCtrl : NavController) {

  }

  ionViewWillEnter() {
    this.myAgenda = this.confAgendaService.getMySessions();
  }

  removeOrNavigate(sessionDetail) {
     const alert = this.alertCtrl.create({
        title : 'View or Remove?',
        message : 'Would you like to view this event or remove it?',
        buttons : [{
          text : 'Navigate',
          handler : () => {
            this.navCtrl.push(this.sessionPage, sessionDetail);
          }
        },
        {
          text : 'Remove',
          handler : () => {
            this.confAgendaService.removeSessionFromAgenda(sessionDetail);
            this.ionViewWillEnter();
          }
        },
        {
          text : 'Cancel',
          'role': 'cancel',
          handler : () => {

          }
        }]
     });
     alert.present();
  }

}
