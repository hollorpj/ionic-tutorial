import { Component, OnInit } from '@angular/core';
import sessionData from '../../data/sessions';
import { ISession } from '../../data/sessions.interface';
import { SessionComponent } from '../session/session';

import { ConfAgendaService } from '../../services/ConfAgendaService';
import { SettingsService } from '../../services/SettingsService';

/**
 * Generated class for the ScheduleComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'schedule',
  templateUrl: 'schedule.html'
})
export class ScheduleComponent implements OnInit {

  sessionCollection : ISession[];
  sessionPage = SessionComponent;

  constructor(private confAgendaService : ConfAgendaService, private settingsService : SettingsService) {

  }

  ngOnInit() {
    this.sessionCollection = sessionData.sort((ls, rs) : number => {
      if (ls.startTime < rs.startTime) return 1;
      if (ls.startTime > rs.startTime) return -1;
      return 0;
    });
  };

  isOnAgenda(session) {
    return this.confAgendaService.doesContainEvent(session);
  }

  addEvent(session) {
    this.confAgendaService.addSessionToAgenda(session);
  }

  removeEvent(session) {
    this.confAgendaService.removeSessionFromAgenda(session);
  }

  getBackground(session : ISession) {
    if (this.confAgendaService.doesContainEvent(session) && this.settingsService.isHighlighted()) {
      return "highlight";
    }
    return "";
  }



}
