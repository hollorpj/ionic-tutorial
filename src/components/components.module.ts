import { NgModule } from '@angular/core';
import { SecondaryComponent } from './secondary/secondary';
import { AgendaComponent } from './agenda/agenda';
import { SessionComponent } from './session/session';
import { ScheduleComponent } from './schedule/schedule';
@NgModule({
	declarations: [SecondaryComponent,
    AgendaComponent,
    SessionComponent,
    ScheduleComponent],
	imports: [],
	exports: [SecondaryComponent,
    AgendaComponent,
    SessionComponent,
    ScheduleComponent]
})
export class ComponentsModule {}
