import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SecondaryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

//@IonicPage()
@Component({
  selector: 'secondary',
  templateUrl: 'secondary.html'
})
export class SecondaryComponent {


  name : string;
  text: string;

  constructor(private navCtrl : NavController, private navParams : NavParams) {
    console.log('Hello SecondaryComponent Component');
    this.name = this.navParams.data.loginName;
    this.text = 'It\'s a me, ' + this.name;
  }

  biteme() {
    this.navCtrl.pop();
  }

}
